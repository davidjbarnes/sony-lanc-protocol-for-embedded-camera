﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Remote
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)

        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Remote))
        Me.Power1 = New System.Windows.Forms.Button
        Me.Stop1 = New System.Windows.Forms.Button
        Me.Play1 = New System.Windows.Forms.Button
        Me.Pause1 = New System.Windows.Forms.Button
        Me.Forward1 = New System.Windows.Forms.Button
        Me.Rewind1 = New System.Windows.Forms.Button
        Me.Eject1 = New System.Windows.Forms.Button
        Me.Forward2 = New System.Windows.Forms.Button
        Me.Rewind2 = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.TapeEnd = New System.Windows.Forms.Button
        Me.Backlight = New System.Windows.Forms.Button
        Me.ZomOut = New System.Windows.Forms.Button
        Me.ZoomIn = New System.Windows.Forms.Button
        Me.SDWrite = New System.Windows.Forms.Button
        Me.PhotoCapture = New System.Windows.Forms.Button
        Me.FocusNear1 = New System.Windows.Forms.Button
        Me.FocusFar1 = New System.Windows.Forms.Button
        Me.Autofocus1 = New System.Windows.Forms.Button
        Me.Record1 = New System.Windows.Forms.Button
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.SignLabel = New System.Windows.Forms.Label
        Me.HoursLabel1 = New System.Windows.Forms.Label
        Me.MinutesLabel1 = New System.Windows.Forms.Label
        Me.SecondsLabel1 = New System.Windows.Forms.Label
        Me.FramesLabel1 = New System.Windows.Forms.Label
        Me.HoursLabel = New System.Windows.Forms.Label
        Me.MinutesLabel = New System.Windows.Forms.Label
        Me.SecondsLabel = New System.Windows.Forms.Label
        Me.FramesLabel = New System.Windows.Forms.Label
        Me.ColonLabel3 = New System.Windows.Forms.Label
        Me.ColonLabel2 = New System.Windows.Forms.Label
        Me.ColonLabel1 = New System.Windows.Forms.Label
        Me.TimecodeLabel1 = New System.Windows.Forms.Label
        Me.BatteryLabel1 = New System.Windows.Forms.Label
        Me.SetUp1 = New System.Windows.Forms.Button
        Me.Assign1 = New System.Windows.Forms.Button
        Me.Assign2 = New System.Windows.Forms.Button
        Me.Assign3 = New System.Windows.Forms.Button
        Me.Assign4 = New System.Windows.Forms.Button
        Me.Assign5 = New System.Windows.Forms.Button
        Me.StatusLabel1 = New System.Windows.Forms.Label
        Me.Assign6 = New System.Windows.Forms.Button
        Me.myport = New System.IO.Ports.SerialPort(Me.components)
        Me.ModeSW1 = New System.Windows.Forms.Button
        Me.ZoomTimer = New System.Windows.Forms.Timer(Me.components)
        Me.FormTimer = New System.Windows.Forms.Timer(Me.components)
        Me.Tenth = New System.Windows.Forms.Button
        Me.Fifth = New System.Windows.Forms.Button
        Me.NineTimes = New System.Windows.Forms.Button
        Me.FrameFWD = New System.Windows.Forms.Button
        Me.FrameREV = New System.Windows.Forms.Button
        Me.TCSWITCH = New System.Windows.Forms.Button
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.DateDisButton2 = New System.Windows.Forms.Button
        Me.DateDisButton1 = New System.Windows.Forms.Button
        Me.CounterButton = New System.Windows.Forms.Button
        Me.DataButton = New System.Windows.Forms.Button
        Me.FocusTimer = New System.Windows.Forms.Timer(Me.components)
        Me.VTRRecord = New System.Windows.Forms.Button
        Me.RecordReview = New System.Windows.Forms.Button
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Power1
        '
        Me.Power1.Location = New System.Drawing.Point(535, 458)
        Me.Power1.Name = "Power1"
        Me.Power1.Size = New System.Drawing.Size(80, 40)
        Me.Power1.TabIndex = 0
        Me.Power1.Text = "Power OFF"
        Me.Power1.UseVisualStyleBackColor = True
        '
        'Stop1
        '
        Me.Stop1.Location = New System.Drawing.Point(229, 404)
        Me.Stop1.Name = "Stop1"
        Me.Stop1.Size = New System.Drawing.Size(57, 25)
        Me.Stop1.TabIndex = 1
        Me.Stop1.Text = "Stop"
        Me.Stop1.UseVisualStyleBackColor = True
        '
        'Play1
        '
        Me.Play1.Location = New System.Drawing.Point(229, 373)
        Me.Play1.Name = "Play1"
        Me.Play1.Size = New System.Drawing.Size(57, 25)
        Me.Play1.TabIndex = 2
        Me.Play1.Text = "Play"
        Me.Play1.UseVisualStyleBackColor = True
        '
        'Pause1
        '
        Me.Pause1.Location = New System.Drawing.Point(416, 373)
        Me.Pause1.Name = "Pause1"
        Me.Pause1.Size = New System.Drawing.Size(57, 25)
        Me.Pause1.TabIndex = 3
        Me.Pause1.Text = "Pause"
        Me.Pause1.UseVisualStyleBackColor = True
        '
        'Forward1
        '
        Me.Forward1.Location = New System.Drawing.Point(292, 373)
        Me.Forward1.Name = "Forward1"
        Me.Forward1.Size = New System.Drawing.Size(57, 25)
        Me.Forward1.TabIndex = 4
        Me.Forward1.Text = ">"
        Me.Forward1.UseVisualStyleBackColor = True
        '
        'Rewind1
        '
        Me.Rewind1.Location = New System.Drawing.Point(166, 373)
        Me.Rewind1.Name = "Rewind1"
        Me.Rewind1.Size = New System.Drawing.Size(57, 25)
        Me.Rewind1.TabIndex = 5
        Me.Rewind1.Text = "<"
        Me.Rewind1.UseVisualStyleBackColor = True
        '
        'Eject1
        '
        Me.Eject1.Location = New System.Drawing.Point(40, 373)
        Me.Eject1.Name = "Eject1"
        Me.Eject1.Size = New System.Drawing.Size(57, 25)
        Me.Eject1.TabIndex = 6
        Me.Eject1.Text = "Eject"
        Me.Eject1.UseVisualStyleBackColor = True
        '
        'Forward2
        '
        Me.Forward2.Location = New System.Drawing.Point(355, 373)
        Me.Forward2.Name = "Forward2"
        Me.Forward2.Size = New System.Drawing.Size(57, 25)
        Me.Forward2.TabIndex = 7
        Me.Forward2.Text = ">>"
        Me.Forward2.UseVisualStyleBackColor = True
        '
        'Rewind2
        '
        Me.Rewind2.Location = New System.Drawing.Point(103, 373)
        Me.Rewind2.Name = "Rewind2"
        Me.Rewind2.Size = New System.Drawing.Size(57, 25)
        Me.Rewind2.TabIndex = 8
        Me.Rewind2.Text = "<<"
        Me.Rewind2.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.RecordReview)
        Me.Panel1.Controls.Add(Me.TapeEnd)
        Me.Panel1.Controls.Add(Me.Backlight)
        Me.Panel1.Controls.Add(Me.ZomOut)
        Me.Panel1.Controls.Add(Me.ZoomIn)
        Me.Panel1.Controls.Add(Me.SDWrite)
        Me.Panel1.Controls.Add(Me.PhotoCapture)
        Me.Panel1.Controls.Add(Me.FocusNear1)
        Me.Panel1.Controls.Add(Me.FocusFar1)
        Me.Panel1.Controls.Add(Me.Autofocus1)
        Me.Panel1.Controls.Add(Me.Record1)
        Me.Panel1.Location = New System.Drawing.Point(24, 134)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(463, 231)
        Me.Panel1.TabIndex = 9
        '
        'TapeEnd
        '
        Me.TapeEnd.Location = New System.Drawing.Point(261, 55)
        Me.TapeEnd.Name = "TapeEnd"
        Me.TapeEnd.Size = New System.Drawing.Size(76, 39)
        Me.TapeEnd.TabIndex = 27
        Me.TapeEnd.Text = "Tape End Search"
        Me.TapeEnd.UseVisualStyleBackColor = True
        '
        'Backlight
        '
        Me.Backlight.Location = New System.Drawing.Point(261, 145)
        Me.Backlight.Name = "Backlight"
        Me.Backlight.Size = New System.Drawing.Size(76, 39)
        Me.Backlight.TabIndex = 26
        Me.Backlight.Text = "Backlight"
        Me.Backlight.UseVisualStyleBackColor = True
        '
        'ZomOut
        '
        Me.ZomOut.Location = New System.Drawing.Point(21, 128)
        Me.ZomOut.Name = "ZomOut"
        Me.ZomOut.Size = New System.Drawing.Size(76, 39)
        Me.ZomOut.TabIndex = 25
        Me.ZomOut.Text = "Zoom Out"
        Me.ZomOut.UseVisualStyleBackColor = True
        '
        'ZoomIn
        '
        Me.ZoomIn.Location = New System.Drawing.Point(21, 61)
        Me.ZoomIn.Name = "ZoomIn"
        Me.ZoomIn.Size = New System.Drawing.Size(76, 39)
        Me.ZoomIn.TabIndex = 24
        Me.ZoomIn.Text = "Zoom In"
        Me.ZoomIn.UseVisualStyleBackColor = True
        '
        'SDWrite
        '
        Me.SDWrite.Location = New System.Drawing.Point(375, 144)
        Me.SDWrite.Name = "SDWrite"
        Me.SDWrite.Size = New System.Drawing.Size(76, 39)
        Me.SDWrite.TabIndex = 23
        Me.SDWrite.Text = "SD Write"
        Me.SDWrite.UseVisualStyleBackColor = True
        '
        'PhotoCapture
        '
        Me.PhotoCapture.Location = New System.Drawing.Point(375, 54)
        Me.PhotoCapture.Name = "PhotoCapture"
        Me.PhotoCapture.Size = New System.Drawing.Size(76, 39)
        Me.PhotoCapture.TabIndex = 22
        Me.PhotoCapture.Text = "Photo Capture"
        Me.PhotoCapture.UseVisualStyleBackColor = True
        '
        'FocusNear1
        '
        Me.FocusNear1.Location = New System.Drawing.Point(144, 145)
        Me.FocusNear1.Name = "FocusNear1"
        Me.FocusNear1.Size = New System.Drawing.Size(76, 39)
        Me.FocusNear1.TabIndex = 16
        Me.FocusNear1.Text = "Focus near"
        Me.FocusNear1.UseVisualStyleBackColor = True
        '
        'FocusFar1
        '
        Me.FocusFar1.Location = New System.Drawing.Point(144, 100)
        Me.FocusFar1.Name = "FocusFar1"
        Me.FocusFar1.Size = New System.Drawing.Size(76, 39)
        Me.FocusFar1.TabIndex = 15
        Me.FocusFar1.Text = "Focus far"
        Me.FocusFar1.UseVisualStyleBackColor = True
        '
        'Autofocus1
        '
        Me.Autofocus1.Location = New System.Drawing.Point(144, 55)
        Me.Autofocus1.Name = "Autofocus1"
        Me.Autofocus1.Size = New System.Drawing.Size(76, 39)
        Me.Autofocus1.TabIndex = 14
        Me.Autofocus1.Text = "Autofocus On/Off"
        Me.Autofocus1.UseVisualStyleBackColor = True
        '
        'Record1
        '
        Me.Record1.BackColor = System.Drawing.Color.GhostWhite
        Me.Record1.Location = New System.Drawing.Point(0, 0)
        Me.Record1.Name = "Record1"
        Me.Record1.Size = New System.Drawing.Size(94, 25)
        Me.Record1.TabIndex = 10
        Me.Record1.Text = "Record"
        Me.Record1.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Black
        Me.Panel2.Controls.Add(Me.SignLabel)
        Me.Panel2.Controls.Add(Me.HoursLabel1)
        Me.Panel2.Controls.Add(Me.MinutesLabel1)
        Me.Panel2.Controls.Add(Me.SecondsLabel1)
        Me.Panel2.Controls.Add(Me.FramesLabel1)
        Me.Panel2.Controls.Add(Me.HoursLabel)
        Me.Panel2.Controls.Add(Me.MinutesLabel)
        Me.Panel2.Controls.Add(Me.SecondsLabel)
        Me.Panel2.Controls.Add(Me.FramesLabel)
        Me.Panel2.Controls.Add(Me.ColonLabel3)
        Me.Panel2.Controls.Add(Me.ColonLabel2)
        Me.Panel2.Controls.Add(Me.ColonLabel1)
        Me.Panel2.Location = New System.Drawing.Point(25, 32)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(431, 80)
        Me.Panel2.TabIndex = 10
        '
        'SignLabel
        '
        Me.SignLabel.AutoSize = True
        Me.SignLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SignLabel.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.SignLabel.Location = New System.Drawing.Point(11, 0)
        Me.SignLabel.MaximumSize = New System.Drawing.Size(20, 60)
        Me.SignLabel.MinimumSize = New System.Drawing.Size(20, 60)
        Me.SignLabel.Name = "SignLabel"
        Me.SignLabel.Size = New System.Drawing.Size(20, 60)
        Me.SignLabel.TabIndex = 23
        Me.SignLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'HoursLabel1
        '
        Me.HoursLabel1.AutoSize = True
        Me.HoursLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HoursLabel1.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.HoursLabel1.Location = New System.Drawing.Point(37, 60)
        Me.HoursLabel1.MaximumSize = New System.Drawing.Size(60, 20)
        Me.HoursLabel1.MinimumSize = New System.Drawing.Size(60, 20)
        Me.HoursLabel1.Name = "HoursLabel1"
        Me.HoursLabel1.Size = New System.Drawing.Size(60, 20)
        Me.HoursLabel1.TabIndex = 22
        Me.HoursLabel1.Text = "Hours"
        Me.HoursLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MinutesLabel1
        '
        Me.MinutesLabel1.AutoSize = True
        Me.MinutesLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinutesLabel1.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.MinutesLabel1.Location = New System.Drawing.Point(137, 60)
        Me.MinutesLabel1.MaximumSize = New System.Drawing.Size(60, 20)
        Me.MinutesLabel1.MinimumSize = New System.Drawing.Size(60, 20)
        Me.MinutesLabel1.Name = "MinutesLabel1"
        Me.MinutesLabel1.Size = New System.Drawing.Size(60, 20)
        Me.MinutesLabel1.TabIndex = 21
        Me.MinutesLabel1.Text = "Minutes"
        Me.MinutesLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SecondsLabel1
        '
        Me.SecondsLabel1.AutoSize = True
        Me.SecondsLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SecondsLabel1.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.SecondsLabel1.Location = New System.Drawing.Point(237, 60)
        Me.SecondsLabel1.MaximumSize = New System.Drawing.Size(60, 20)
        Me.SecondsLabel1.MinimumSize = New System.Drawing.Size(60, 20)
        Me.SecondsLabel1.Name = "SecondsLabel1"
        Me.SecondsLabel1.Size = New System.Drawing.Size(60, 20)
        Me.SecondsLabel1.TabIndex = 20
        Me.SecondsLabel1.Text = "Seconds"
        Me.SecondsLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FramesLabel1
        '
        Me.FramesLabel1.AutoSize = True
        Me.FramesLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FramesLabel1.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.FramesLabel1.Location = New System.Drawing.Point(337, 60)
        Me.FramesLabel1.MaximumSize = New System.Drawing.Size(60, 20)
        Me.FramesLabel1.MinimumSize = New System.Drawing.Size(60, 20)
        Me.FramesLabel1.Name = "FramesLabel1"
        Me.FramesLabel1.Size = New System.Drawing.Size(60, 20)
        Me.FramesLabel1.TabIndex = 19
        Me.FramesLabel1.Text = "Frames"
        Me.FramesLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'HoursLabel
        '
        Me.HoursLabel.AutoSize = True
        Me.HoursLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HoursLabel.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.HoursLabel.Location = New System.Drawing.Point(37, 0)
        Me.HoursLabel.MaximumSize = New System.Drawing.Size(60, 60)
        Me.HoursLabel.MinimumSize = New System.Drawing.Size(60, 60)
        Me.HoursLabel.Name = "HoursLabel"
        Me.HoursLabel.Size = New System.Drawing.Size(60, 60)
        Me.HoursLabel.TabIndex = 18
        Me.HoursLabel.Text = "--"
        Me.HoursLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MinutesLabel
        '
        Me.MinutesLabel.AutoSize = True
        Me.MinutesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinutesLabel.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.MinutesLabel.Location = New System.Drawing.Point(137, 0)
        Me.MinutesLabel.MaximumSize = New System.Drawing.Size(60, 60)
        Me.MinutesLabel.MinimumSize = New System.Drawing.Size(60, 60)
        Me.MinutesLabel.Name = "MinutesLabel"
        Me.MinutesLabel.Size = New System.Drawing.Size(60, 60)
        Me.MinutesLabel.TabIndex = 17
        Me.MinutesLabel.Text = "--"
        Me.MinutesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SecondsLabel
        '
        Me.SecondsLabel.AutoSize = True
        Me.SecondsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SecondsLabel.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.SecondsLabel.Location = New System.Drawing.Point(237, 0)
        Me.SecondsLabel.MaximumSize = New System.Drawing.Size(60, 60)
        Me.SecondsLabel.MinimumSize = New System.Drawing.Size(60, 60)
        Me.SecondsLabel.Name = "SecondsLabel"
        Me.SecondsLabel.Size = New System.Drawing.Size(60, 60)
        Me.SecondsLabel.TabIndex = 16
        Me.SecondsLabel.Text = "--"
        Me.SecondsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FramesLabel
        '
        Me.FramesLabel.AutoSize = True
        Me.FramesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FramesLabel.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.FramesLabel.Location = New System.Drawing.Point(337, 0)
        Me.FramesLabel.MaximumSize = New System.Drawing.Size(60, 60)
        Me.FramesLabel.MinimumSize = New System.Drawing.Size(60, 60)
        Me.FramesLabel.Name = "FramesLabel"
        Me.FramesLabel.Size = New System.Drawing.Size(60, 60)
        Me.FramesLabel.TabIndex = 15
        Me.FramesLabel.Text = "--"
        Me.FramesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ColonLabel3
        '
        Me.ColonLabel3.AutoSize = True
        Me.ColonLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColonLabel3.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.ColonLabel3.Location = New System.Drawing.Point(97, 0)
        Me.ColonLabel3.MaximumSize = New System.Drawing.Size(40, 60)
        Me.ColonLabel3.MinimumSize = New System.Drawing.Size(40, 60)
        Me.ColonLabel3.Name = "ColonLabel3"
        Me.ColonLabel3.Size = New System.Drawing.Size(40, 60)
        Me.ColonLabel3.TabIndex = 14
        Me.ColonLabel3.Text = ":"
        Me.ColonLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ColonLabel2
        '
        Me.ColonLabel2.AutoSize = True
        Me.ColonLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColonLabel2.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.ColonLabel2.Location = New System.Drawing.Point(197, 0)
        Me.ColonLabel2.MaximumSize = New System.Drawing.Size(40, 60)
        Me.ColonLabel2.MinimumSize = New System.Drawing.Size(40, 60)
        Me.ColonLabel2.Name = "ColonLabel2"
        Me.ColonLabel2.Size = New System.Drawing.Size(40, 60)
        Me.ColonLabel2.TabIndex = 13
        Me.ColonLabel2.Text = ":"
        Me.ColonLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ColonLabel1
        '
        Me.ColonLabel1.AutoSize = True
        Me.ColonLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColonLabel1.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.ColonLabel1.Location = New System.Drawing.Point(297, 0)
        Me.ColonLabel1.MaximumSize = New System.Drawing.Size(40, 60)
        Me.ColonLabel1.MinimumSize = New System.Drawing.Size(40, 60)
        Me.ColonLabel1.Name = "ColonLabel1"
        Me.ColonLabel1.Size = New System.Drawing.Size(40, 60)
        Me.ColonLabel1.TabIndex = 12
        Me.ColonLabel1.Text = ":"
        Me.ColonLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimecodeLabel1
        '
        Me.TimecodeLabel1.AutoSize = True
        Me.TimecodeLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TimecodeLabel1.ForeColor = System.Drawing.Color.White
        Me.TimecodeLabel1.Location = New System.Drawing.Point(22, 5)
        Me.TimecodeLabel1.Name = "TimecodeLabel1"
        Me.TimecodeLabel1.Size = New System.Drawing.Size(104, 24)
        Me.TimecodeLabel1.TabIndex = 11
        Me.TimecodeLabel1.Text = "Timecode"
        '
        'BatteryLabel1
        '
        Me.BatteryLabel1.AutoSize = True
        Me.BatteryLabel1.BackColor = System.Drawing.Color.DarkSalmon
        Me.BatteryLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BatteryLabel1.Location = New System.Drawing.Point(530, 92)
        Me.BatteryLabel1.MaximumSize = New System.Drawing.Size(150, 40)
        Me.BatteryLabel1.MinimumSize = New System.Drawing.Size(150, 40)
        Me.BatteryLabel1.Name = "BatteryLabel1"
        Me.BatteryLabel1.Size = New System.Drawing.Size(150, 40)
        Me.BatteryLabel1.TabIndex = 21
        Me.BatteryLabel1.Text = "Battery Low"
        Me.BatteryLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BatteryLabel1.Visible = False
        '
        'SetUp1
        '
        Me.SetUp1.Location = New System.Drawing.Point(547, 381)
        Me.SetUp1.Name = "SetUp1"
        Me.SetUp1.Size = New System.Drawing.Size(57, 25)
        Me.SetUp1.TabIndex = 22
        Me.SetUp1.Text = "Set up"
        Me.SetUp1.UseVisualStyleBackColor = True
        '
        'Assign1
        '
        Me.Assign1.Location = New System.Drawing.Point(610, 165)
        Me.Assign1.Name = "Assign1"
        Me.Assign1.Size = New System.Drawing.Size(57, 25)
        Me.Assign1.TabIndex = 23
        Me.Assign1.Text = "Assign1"
        Me.Assign1.UseVisualStyleBackColor = True
        '
        'Assign2
        '
        Me.Assign2.Location = New System.Drawing.Point(610, 196)
        Me.Assign2.Name = "Assign2"
        Me.Assign2.Size = New System.Drawing.Size(57, 25)
        Me.Assign2.TabIndex = 24
        Me.Assign2.Text = "Assign 2"
        Me.Assign2.UseVisualStyleBackColor = True
        '
        'Assign3
        '
        Me.Assign3.Location = New System.Drawing.Point(610, 227)
        Me.Assign3.Name = "Assign3"
        Me.Assign3.Size = New System.Drawing.Size(57, 25)
        Me.Assign3.TabIndex = 25
        Me.Assign3.Text = "Assign 3"
        Me.Assign3.UseVisualStyleBackColor = True
        '
        'Assign4
        '
        Me.Assign4.Location = New System.Drawing.Point(610, 258)
        Me.Assign4.Name = "Assign4"
        Me.Assign4.Size = New System.Drawing.Size(57, 25)
        Me.Assign4.TabIndex = 26
        Me.Assign4.Text = "Assign 4"
        Me.Assign4.UseVisualStyleBackColor = True
        '
        'Assign5
        '
        Me.Assign5.Location = New System.Drawing.Point(610, 293)
        Me.Assign5.Name = "Assign5"
        Me.Assign5.Size = New System.Drawing.Size(57, 25)
        Me.Assign5.TabIndex = 27
        Me.Assign5.Text = "Assign 5"
        Me.Assign5.UseVisualStyleBackColor = True
        '
        'StatusLabel1
        '
        Me.StatusLabel1.AutoSize = True
        Me.StatusLabel1.BackColor = System.Drawing.Color.BurlyWood
        Me.StatusLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusLabel1.Location = New System.Drawing.Point(530, 40)
        Me.StatusLabel1.MaximumSize = New System.Drawing.Size(150, 60)
        Me.StatusLabel1.MinimumSize = New System.Drawing.Size(150, 60)
        Me.StatusLabel1.Name = "StatusLabel1"
        Me.StatusLabel1.Size = New System.Drawing.Size(150, 60)
        Me.StatusLabel1.TabIndex = 28
        Me.StatusLabel1.Text = "test"
        Me.StatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Assign6
        '
        Me.Assign6.Location = New System.Drawing.Point(610, 324)
        Me.Assign6.Name = "Assign6"
        Me.Assign6.Size = New System.Drawing.Size(57, 25)
        Me.Assign6.TabIndex = 29
        Me.Assign6.Text = "Assign 6"
        Me.Assign6.UseVisualStyleBackColor = True
        '
        'myport
        '
        Me.myport.PortName = "COM3"
        '
        'ModeSW1
        '
        Me.ModeSW1.Location = New System.Drawing.Point(535, 412)
        Me.ModeSW1.Name = "ModeSW1"
        Me.ModeSW1.Size = New System.Drawing.Size(80, 40)
        Me.ModeSW1.TabIndex = 30
        Me.ModeSW1.Text = "Mode Switch"
        Me.ModeSW1.UseVisualStyleBackColor = True
        '
        'ZoomTimer
        '
        Me.ZoomTimer.Interval = 20
        '
        'FormTimer
        '
        '
        'Tenth
        '
        Me.Tenth.Location = New System.Drawing.Point(165, 447)
        Me.Tenth.Name = "Tenth"
        Me.Tenth.Size = New System.Drawing.Size(57, 25)
        Me.Tenth.TabIndex = 31
        Me.Tenth.Text = "x1/10"
        Me.Tenth.UseVisualStyleBackColor = True
        '
        'Fifth
        '
        Me.Fifth.Location = New System.Drawing.Point(228, 447)
        Me.Fifth.Name = "Fifth"
        Me.Fifth.Size = New System.Drawing.Size(57, 25)
        Me.Fifth.TabIndex = 32
        Me.Fifth.Text = "x1/5"
        Me.Fifth.UseVisualStyleBackColor = True
        '
        'NineTimes
        '
        Me.NineTimes.Location = New System.Drawing.Point(292, 447)
        Me.NineTimes.Name = "NineTimes"
        Me.NineTimes.Size = New System.Drawing.Size(57, 25)
        Me.NineTimes.TabIndex = 33
        Me.NineTimes.Text = "x9"
        Me.NineTimes.UseVisualStyleBackColor = True
        '
        'FrameFWD
        '
        Me.FrameFWD.Location = New System.Drawing.Point(355, 447)
        Me.FrameFWD.Name = "FrameFWD"
        Me.FrameFWD.Size = New System.Drawing.Size(118, 25)
        Me.FrameFWD.TabIndex = 34
        Me.FrameFWD.Text = "Frame FWD"
        Me.FrameFWD.UseVisualStyleBackColor = True
        '
        'FrameREV
        '
        Me.FrameREV.Location = New System.Drawing.Point(40, 447)
        Me.FrameREV.Name = "FrameREV"
        Me.FrameREV.Size = New System.Drawing.Size(118, 25)
        Me.FrameREV.TabIndex = 35
        Me.FrameREV.Text = "Frame REV"
        Me.FrameREV.UseVisualStyleBackColor = True
        '
        'TCSWITCH
        '
        Me.TCSWITCH.Location = New System.Drawing.Point(530, 7)
        Me.TCSWITCH.Name = "TCSWITCH"
        Me.TCSWITCH.Size = New System.Drawing.Size(150, 25)
        Me.TCSWITCH.TabIndex = 36
        Me.TCSWITCH.Text = "TC / RC reset"
        Me.TCSWITCH.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.MistyRose
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.DateDisButton2)
        Me.Panel3.Controls.Add(Me.DateDisButton1)
        Me.Panel3.Controls.Add(Me.CounterButton)
        Me.Panel3.Controls.Add(Me.DataButton)
        Me.Panel3.Location = New System.Drawing.Point(486, 134)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(117, 231)
        Me.Panel3.TabIndex = 37
        '
        'DateDisButton2
        '
        Me.DateDisButton2.Location = New System.Drawing.Point(5, 187)
        Me.DateDisButton2.Name = "DateDisButton2"
        Me.DateDisButton2.Size = New System.Drawing.Size(107, 28)
        Me.DateDisButton2.TabIndex = 3
        Me.DateDisButton2.Text = "Date Display OFF"
        Me.DateDisButton2.UseVisualStyleBackColor = True
        '
        'DateDisButton1
        '
        Me.DateDisButton1.Location = New System.Drawing.Point(5, 145)
        Me.DateDisButton1.Name = "DateDisButton1"
        Me.DateDisButton1.Size = New System.Drawing.Size(107, 28)
        Me.DateDisButton1.TabIndex = 2
        Me.DateDisButton1.Text = "Date Display ON"
        Me.DateDisButton1.UseVisualStyleBackColor = True
        '
        'CounterButton
        '
        Me.CounterButton.Location = New System.Drawing.Point(3, 59)
        Me.CounterButton.Name = "CounterButton"
        Me.CounterButton.Size = New System.Drawing.Size(107, 28)
        Me.CounterButton.TabIndex = 1
        Me.CounterButton.Text = "V/F Data  Display"
        Me.CounterButton.UseVisualStyleBackColor = True
        '
        'DataButton
        '
        Me.DataButton.Location = New System.Drawing.Point(5, 105)
        Me.DataButton.Name = "DataButton"
        Me.DataButton.Size = New System.Drawing.Size(107, 28)
        Me.DataButton.TabIndex = 0
        Me.DataButton.Text = "Data Display"
        Me.DataButton.UseVisualStyleBackColor = True
        '
        'FocusTimer
        '
        '
        'VTRRecord
        '
        Me.VTRRecord.Location = New System.Drawing.Point(292, 404)
        Me.VTRRecord.Name = "VTRRecord"
        Me.VTRRecord.Size = New System.Drawing.Size(120, 28)
        Me.VTRRecord.TabIndex = 4
        Me.VTRRecord.Text = "VTR Record"
        Me.VTRRecord.UseVisualStyleBackColor = True
        '
        'RecordReview
        '
        Me.RecordReview.Location = New System.Drawing.Point(261, 100)
        Me.RecordReview.Name = "RecordReview"
        Me.RecordReview.Size = New System.Drawing.Size(76, 39)
        Me.RecordReview.TabIndex = 28
        Me.RecordReview.Text = "Record Review"
        Me.RecordReview.UseVisualStyleBackColor = True
        '
        'Remote
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(692, 510)
        Me.Controls.Add(Me.VTRRecord)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.TCSWITCH)
        Me.Controls.Add(Me.FrameREV)
        Me.Controls.Add(Me.FrameFWD)
        Me.Controls.Add(Me.NineTimes)
        Me.Controls.Add(Me.Fifth)
        Me.Controls.Add(Me.Tenth)
        Me.Controls.Add(Me.ModeSW1)
        Me.Controls.Add(Me.Assign6)
        Me.Controls.Add(Me.StatusLabel1)
        Me.Controls.Add(Me.Assign5)
        Me.Controls.Add(Me.Assign4)
        Me.Controls.Add(Me.Assign3)
        Me.Controls.Add(Me.Assign2)
        Me.Controls.Add(Me.Assign1)
        Me.Controls.Add(Me.SetUp1)
        Me.Controls.Add(Me.BatteryLabel1)
        Me.Controls.Add(Me.TimecodeLabel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Rewind2)
        Me.Controls.Add(Me.Forward2)
        Me.Controls.Add(Me.Eject1)
        Me.Controls.Add(Me.Rewind1)
        Me.Controls.Add(Me.Forward1)
        Me.Controls.Add(Me.Pause1)
        Me.Controls.Add(Me.Play1)
        Me.Controls.Add(Me.Stop1)
        Me.Controls.Add(Me.Power1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Remote"
        Me.Text = "Sony Remote Control by Nigel Ealand"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Power1 As System.Windows.Forms.Button
    Friend WithEvents Stop1 As System.Windows.Forms.Button
    Friend WithEvents Play1 As System.Windows.Forms.Button
    Friend WithEvents Pause1 As System.Windows.Forms.Button
    Friend WithEvents Forward1 As System.Windows.Forms.Button
    Friend WithEvents Rewind1 As System.Windows.Forms.Button
    Friend WithEvents Eject1 As System.Windows.Forms.Button
    Friend WithEvents Forward2 As System.Windows.Forms.Button
    Friend WithEvents Rewind2 As System.Windows.Forms.Button
    Friend WithEvents Record1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Autofocus1 As System.Windows.Forms.Button
    Friend WithEvents FocusNear1 As System.Windows.Forms.Button
    Friend WithEvents FocusFar1 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TimecodeLabel1 As System.Windows.Forms.Label
    Friend WithEvents ColonLabel1 As System.Windows.Forms.Label
    Friend WithEvents HoursLabel As System.Windows.Forms.Label
    Friend WithEvents MinutesLabel As System.Windows.Forms.Label
    Friend WithEvents SecondsLabel As System.Windows.Forms.Label
    Friend WithEvents FramesLabel As System.Windows.Forms.Label
    Friend WithEvents ColonLabel3 As System.Windows.Forms.Label
    Friend WithEvents ColonLabel2 As System.Windows.Forms.Label
    Friend WithEvents SecondsLabel1 As System.Windows.Forms.Label
    Friend WithEvents FramesLabel1 As System.Windows.Forms.Label
    Friend WithEvents HoursLabel1 As System.Windows.Forms.Label
    Friend WithEvents MinutesLabel1 As System.Windows.Forms.Label
    Friend WithEvents BatteryLabel1 As System.Windows.Forms.Label
    Friend WithEvents SetUp1 As System.Windows.Forms.Button
    Friend WithEvents Assign1 As System.Windows.Forms.Button
    Friend WithEvents Assign2 As System.Windows.Forms.Button
    Friend WithEvents Assign3 As System.Windows.Forms.Button
    Friend WithEvents Assign4 As System.Windows.Forms.Button
    Friend WithEvents Assign5 As System.Windows.Forms.Button
    Friend WithEvents StatusLabel1 As System.Windows.Forms.Label
    Friend WithEvents SignLabel As System.Windows.Forms.Label
    Friend WithEvents SDWrite As System.Windows.Forms.Button
    Friend WithEvents PhotoCapture As System.Windows.Forms.Button
    Friend WithEvents Assign6 As System.Windows.Forms.Button
    Friend WithEvents myport As System.IO.Ports.SerialPort
    Friend WithEvents ModeSW1 As System.Windows.Forms.Button
    Friend WithEvents ZoomTimer As System.Windows.Forms.Timer
    Friend WithEvents ZomOut As System.Windows.Forms.Button
    Friend WithEvents ZoomIn As System.Windows.Forms.Button
    Friend WithEvents FormTimer As System.Windows.Forms.Timer
    Friend WithEvents Backlight As System.Windows.Forms.Button
    Friend WithEvents Tenth As System.Windows.Forms.Button
    Friend WithEvents Fifth As System.Windows.Forms.Button
    Friend WithEvents NineTimes As System.Windows.Forms.Button
    Friend WithEvents FrameFWD As System.Windows.Forms.Button
    Friend WithEvents FrameREV As System.Windows.Forms.Button
    Friend WithEvents TCSWITCH As System.Windows.Forms.Button
    Friend WithEvents TapeEnd As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents DataButton As System.Windows.Forms.Button
    Friend WithEvents DateDisButton2 As System.Windows.Forms.Button
    Friend WithEvents DateDisButton1 As System.Windows.Forms.Button
    Friend WithEvents CounterButton As System.Windows.Forms.Button
    Friend WithEvents FocusTimer As System.Windows.Forms.Timer
    Friend WithEvents RecordReview As System.Windows.Forms.Button
    Friend WithEvents VTRRecord As System.Windows.Forms.Button

End Class
