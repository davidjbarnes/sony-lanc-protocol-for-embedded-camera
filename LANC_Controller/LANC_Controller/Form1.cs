using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace LANC_Controller
{
    class LANC
    {
        [DllImport("Lanc32.dll")]
        public static extern Int16 OpenLancDeviceEx(ref Int32 LancDevice, String ComPort, Int32 dwCallback, Int32 dwFlags);

        [DllImport("Lanc32.dll")]
        public static extern bool CloseLancDevice(ref Int32 LancDevice);

        [DllImport("Lanc32.dll")]
        
        public static extern bool SendLancCommand(ref Int32 LancDevice, byte MessageType, byte Message);

    };

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private const byte COMMAND_CAMERA = 0x28;
        private const byte COMMAND_ZOOM_IN_SLOW = 0x35;
        private const byte COMMAND_ZOOM_OUT_SLOW = 0x37;

        private void Form1_Load(object sender, EventArgs e)
        {
            Int32 MyLancDevice = 0;

            LANC.OpenLancDeviceEx(ref MyLancDevice, "COM1", 0, 0);
            LANC.SendLancCommand(ref MyLancDevice, COMMAND_CAMERA, COMMAND_ZOOM_IN_SLOW);

            LANC.CloseLancDevice(ref MyLancDevice);
        }

        
        
    }
}