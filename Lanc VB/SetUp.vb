﻿Public Class SetUp

    Public Sub New()

        ' Creates this form for the first time
        InitializeComponent()

        'Loads default codes
        DefaultValues()

        'Find the ports available
        GetSerialPortNames()

        If ListBox1.Items(0).ToString <> "" Then
            ComPortName = ListBox1.Items(0)
            ListBox1.SelectedItem = ComPortName
        Else
            MsgBox("There are no Comm Ports available")
        End If
        ' Add any initialization after the InitializeComponent() call.

    End Sub


    Sub GetSerialPortNames()
        ' Show all available COM ports in Listbox1. 
        For Each sp As String In My.Computer.Ports.SerialPortNames
            ListBox1.Items.Add(sp)
        Next

    End Sub


    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        ComPortName = ListBox1.SelectedItem.ToString
        
        Initial_Port()

    End Sub

    Private Sub SetDefault_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SetDefault.Click
        DefaultValues()
    End Sub

    Private Sub GoControls1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GoControls1.Click


        Loadvariables()

        If ComPortName <> "" Then
            ListBox1.SelectedItem = ComPortName
        Else
            ComPortName = ListBox1.Items(0)
            MsgBox(ComPortName)
        End If

        Remote.Visible = True
        Me.Visible = False

    End Sub

    Private Sub PrintForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrintForm.Click
        PrintDocument1.Print()

    End Sub

    Private Sub DefaultValues()
        'This reloads all the default codes for the camera
        Me.TextBox1.Text = "1833"
        Me.TextBox2.Text = "2851"
        Me.TextBox3.Text = "2804"
        Me.TextBox4.Text = "2802"
        Me.TextBox5.Text = "2800"
        Me.TextBox6.Text = "2810"
        Me.TextBox7.Text = "2812"
        Me.TextBox8.Text = "2814"
        Me.TextBox9.Text = "2841"
        Me.TextBox10.Text = "2845"
        Me.TextBox11.Text = "2847"
        Me.TextBox12.Text = ""
        Me.TextBox13.Text = ""
        Me.TextBox14.Text = ""
        Me.TextBox15.Text = ""
        Me.TextBox16.Text = ""
        Me.TextBox17.Text = ""
        Me.TextBox18.Text = "1836"
        Me.TextBox19.Text = "1850"
        Me.TextBox20.Text = "1834"
        Me.TextBox21.Text = "1852"
        Me.TextBox22.Text = "1838"
        Me.TextBox23.Text = "1840"
        Me.TextBox24.Text = "1830"
        Me.TextBox25.Text = "ATSP"
        Me.TextBox26.Text = "105E"
        Me.TextBox27.Text = ""
        Me.TextBox28.Text = ""
        Me.TextBox29.Text = ""
        Me.TextBox30.Text = ""
        Me.TextBox31.Text = ""
        Me.TextBox32.Text = ""
        Me.TextBox33.Text = "1805"
        Me.TextBox34.Text = "1839"
        Me.TextBox35.Text = "182B"
        Me.TextBox36.Text = "1844"
        Me.TextBox37.Text = "1846"
        Me.TextBox38.Text = "184C"
        Me.TextBox39.Text = "1860"
        Me.TextBox40.Text = "1862"
        Me.TextBox41.Text = "188E"
        Me.TextBox42.Text = "187B"
        Me.TextBox43.Text = "1899"
        Me.TextBox44.Text = "18B4"
        Me.TextBox45.Text = "18BF"
        Me.TextBox46.Text = "18BD"
        Me.TextBox47.Text = "1869"
        Me.TextBox48.Text = "183A"


    End Sub

    Public Sub Loadvariables()
        'This assigns the codes to recognisable variables
        MODE_REC = Me.TextBox1.Text
        Mode_Backlight = Me.TextBox2.Text
        MODE_TELE3 = Me.TextBox3.Text()
        MODE_TELE2 = Me.TextBox4.Text
        MODE_TELE1 = Me.TextBox5.Text
        MODE_WIDE1 = Me.TextBox6.Text
        MODE_WIDE2 = Me.TextBox7.Text
        MODE_WIDE3 = Me.TextBox8.Text
        MODE_AutoFocus = Me.TextBox9.Text
        MODE_FocusFar = Me.TextBox10.Text
        MODE_FocusNear = Me.TextBox11.Text
        MODE_AutoIris = Me.TextBox12.Text
        MODE_IrisUp = Me.TextBox13.Text
        MODE_IrisDown = Me.TextBox14.Text
        MODE_AutoWB = Me.TextBox15.Text
        MODE_WBSet = Me.TextBox16.Text
        MODE_EJECT = Me.TextBox17.Text
        MODE_REWIND = Me.TextBox18.Text
        MODE_REVIEW = Me.TextBox19.Text
        MODE_PLAY = Me.TextBox20.Text
        MODE_CUE_ON = Me.TextBox21.Text
        MODE_FF = Me.TextBox22.Text
        MODE_PAUSE = Me.TextBox23.Text
        MODE_STOP = Me.TextBox24.Text
        MODE_ON = Me.TextBox25.Text
        MODE_OFF = Me.TextBox26.Text
        MODE_Assign1 = Me.TextBox27.Text
        MODE_Assign2 = Me.TextBox28.Text
        MODE_Assign3 = Me.TextBox29.Text
        MODE_Assign4 = Me.TextBox30.Text
        MODE_Assign5 = Me.TextBox31.Text
        MODE_Assign6 = Me.TextBox32.Text
        MODE_Switch = Me.TextBox33.Text
        Mode_PhotoCapture = Me.TextBox34.Text
        MODE_SDWrite = Me.TextBox35.Text
        MODE_TENTH = Me.TextBox36.Text
        MODE_FIFTH = Me.TextBox37.Text
        MODE_NINE = Me.TextBox38.Text
        MODE_FRAMEREV = Me.TextBox39.Text
        MODE_FRAMEFWD = Me.TextBox40.Text
        MODE_TCRC = Me.TextBox41.Text
        MODE_TAPE_END = Me.TextBox42.Text
        MODE_DATA = Me.TextBox43.Text
        MODE_COUNTER = Me.TextBox44.Text
        MODE_DATEON = Me.TextBox45.Text
        MODE_DATEOFF = Me.TextBox46.Text
        MODE_RecReview = Me.TextBox47.Text
        MODE_VTRRec = Me.TextBox48.Text

    End Sub


    Private Sub SetUp_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Remote.Close()
        close_port()
    End Sub
End Class