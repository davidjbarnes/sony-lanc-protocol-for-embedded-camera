﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SetUp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SetUp))
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label43 = New System.Windows.Forms.Label
        Me.TextBox42 = New System.Windows.Forms.TextBox
        Me.Label42 = New System.Windows.Forms.Label
        Me.TextBox41 = New System.Windows.Forms.TextBox
        Me.TextBox16 = New System.Windows.Forms.TextBox
        Me.TextBox15 = New System.Windows.Forms.TextBox
        Me.TextBox14 = New System.Windows.Forms.TextBox
        Me.TextBox13 = New System.Windows.Forms.TextBox
        Me.TextBox12 = New System.Windows.Forms.TextBox
        Me.TextBox11 = New System.Windows.Forms.TextBox
        Me.TextBox10 = New System.Windows.Forms.TextBox
        Me.TextBox9 = New System.Windows.Forms.TextBox
        Me.TextBox8 = New System.Windows.Forms.TextBox
        Me.TextBox7 = New System.Windows.Forms.TextBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label41 = New System.Windows.Forms.Label
        Me.Label40 = New System.Windows.Forms.Label
        Me.TextBox40 = New System.Windows.Forms.TextBox
        Me.TextBox39 = New System.Windows.Forms.TextBox
        Me.Label39 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.Label37 = New System.Windows.Forms.Label
        Me.TextBox38 = New System.Windows.Forms.TextBox
        Me.TextBox37 = New System.Windows.Forms.TextBox
        Me.TextBox36 = New System.Windows.Forms.TextBox
        Me.Label36 = New System.Windows.Forms.Label
        Me.TextBox35 = New System.Windows.Forms.TextBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.TextBox34 = New System.Windows.Forms.TextBox
        Me.Label34 = New System.Windows.Forms.Label
        Me.TextBox33 = New System.Windows.Forms.TextBox
        Me.TextBox26 = New System.Windows.Forms.TextBox
        Me.TextBox25 = New System.Windows.Forms.TextBox
        Me.TextBox24 = New System.Windows.Forms.TextBox
        Me.TextBox23 = New System.Windows.Forms.TextBox
        Me.TextBox22 = New System.Windows.Forms.TextBox
        Me.TextBox21 = New System.Windows.Forms.TextBox
        Me.TextBox20 = New System.Windows.Forms.TextBox
        Me.TextBox19 = New System.Windows.Forms.TextBox
        Me.TextBox18 = New System.Windows.Forms.TextBox
        Me.TextBox17 = New System.Windows.Forms.TextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label47 = New System.Windows.Forms.Label
        Me.TextBox46 = New System.Windows.Forms.TextBox
        Me.Label46 = New System.Windows.Forms.Label
        Me.TextBox45 = New System.Windows.Forms.TextBox
        Me.Label45 = New System.Windows.Forms.Label
        Me.TextBox44 = New System.Windows.Forms.TextBox
        Me.Label44 = New System.Windows.Forms.Label
        Me.TextBox32 = New System.Windows.Forms.TextBox
        Me.TextBox43 = New System.Windows.Forms.TextBox
        Me.Label32 = New System.Windows.Forms.Label
        Me.TextBox31 = New System.Windows.Forms.TextBox
        Me.TextBox30 = New System.Windows.Forms.TextBox
        Me.TextBox29 = New System.Windows.Forms.TextBox
        Me.TextBox28 = New System.Windows.Forms.TextBox
        Me.TextBox27 = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.SetDefault = New System.Windows.Forms.Button
        Me.GoControls1 = New System.Windows.Forms.Button
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Label33 = New System.Windows.Forms.Label
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.PrintForm = New System.Windows.Forms.Button
        Me.Label48 = New System.Windows.Forms.Label
        Me.TextBox47 = New System.Windows.Forms.TextBox
        Me.Label49 = New System.Windows.Forms.Label
        Me.TextBox48 = New System.Windows.Forms.TextBox
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label1.Location = New System.Drawing.Point(66, 2)
        Me.Label1.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label1.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 20)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Record"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(16, 3)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(34, 20)
        Me.TextBox1.TabIndex = 23
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.BurlyWood
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label48)
        Me.Panel1.Controls.Add(Me.Label43)
        Me.Panel1.Controls.Add(Me.TextBox47)
        Me.Panel1.Controls.Add(Me.TextBox42)
        Me.Panel1.Controls.Add(Me.Label42)
        Me.Panel1.Controls.Add(Me.TextBox41)
        Me.Panel1.Controls.Add(Me.TextBox16)
        Me.Panel1.Controls.Add(Me.TextBox15)
        Me.Panel1.Controls.Add(Me.TextBox14)
        Me.Panel1.Controls.Add(Me.TextBox13)
        Me.Panel1.Controls.Add(Me.TextBox12)
        Me.Panel1.Controls.Add(Me.TextBox11)
        Me.Panel1.Controls.Add(Me.TextBox10)
        Me.Panel1.Controls.Add(Me.TextBox9)
        Me.Panel1.Controls.Add(Me.TextBox8)
        Me.Panel1.Controls.Add(Me.TextBox7)
        Me.Panel1.Controls.Add(Me.TextBox6)
        Me.Panel1.Controls.Add(Me.TextBox5)
        Me.Panel1.Controls.Add(Me.TextBox4)
        Me.Panel1.Controls.Add(Me.TextBox3)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Location = New System.Drawing.Point(22, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(167, 505)
        Me.Panel1.TabIndex = 29
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label43.Location = New System.Drawing.Point(66, 445)
        Me.Label43.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label43.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(100, 20)
        Me.Label43.TabIndex = 112
        Me.Label43.Text = "Tep End Search"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox42
        '
        Me.TextBox42.Location = New System.Drawing.Point(15, 446)
        Me.TextBox42.Name = "TextBox42"
        Me.TextBox42.Size = New System.Drawing.Size(34, 20)
        Me.TextBox42.TabIndex = 111
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label42.Location = New System.Drawing.Point(66, 419)
        Me.Label42.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label42.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(100, 20)
        Me.Label42.TabIndex = 110
        Me.Label42.Text = "TC / RC reset"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox41
        '
        Me.TextBox41.Location = New System.Drawing.Point(15, 420)
        Me.TextBox41.Name = "TextBox41"
        Me.TextBox41.Size = New System.Drawing.Size(34, 20)
        Me.TextBox41.TabIndex = 109
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(16, 394)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(34, 20)
        Me.TextBox16.TabIndex = 73
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(16, 367)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(34, 20)
        Me.TextBox15.TabIndex = 72
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(16, 341)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(34, 20)
        Me.TextBox14.TabIndex = 71
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(16, 315)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(34, 20)
        Me.TextBox13.TabIndex = 70
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(16, 288)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(34, 20)
        Me.TextBox12.TabIndex = 69
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(16, 263)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(34, 20)
        Me.TextBox11.TabIndex = 68
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(16, 237)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(34, 20)
        Me.TextBox10.TabIndex = 67
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(16, 210)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(34, 20)
        Me.TextBox9.TabIndex = 66
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(16, 185)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(34, 20)
        Me.TextBox8.TabIndex = 65
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(16, 159)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(34, 20)
        Me.TextBox7.TabIndex = 64
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(16, 131)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(34, 20)
        Me.TextBox6.TabIndex = 63
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(16, 105)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(34, 20)
        Me.TextBox5.TabIndex = 62
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(16, 79)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(34, 20)
        Me.TextBox4.TabIndex = 61
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(16, 55)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(34, 20)
        Me.TextBox3.TabIndex = 60
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(16, 29)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(34, 20)
        Me.TextBox2.TabIndex = 59
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label16.Location = New System.Drawing.Point(66, 393)
        Me.Label16.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label16.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 20)
        Me.Label16.TabIndex = 58
        Me.Label16.Text = "White Bal Set"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.Location = New System.Drawing.Point(66, 132)
        Me.Label6.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label6.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 20)
        Me.Label6.TabIndex = 48
        Me.Label6.Text = "Zoom Near 1"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label15.Location = New System.Drawing.Point(66, 367)
        Me.Label15.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label15.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 20)
        Me.Label15.TabIndex = 57
        Me.Label15.Text = "Auto White Bal"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.Location = New System.Drawing.Point(66, 184)
        Me.Label8.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label8.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 20)
        Me.Label8.TabIndex = 50
        Me.Label8.Text = "Zoom Near 3"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Location = New System.Drawing.Point(66, 28)
        Me.Label2.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label2.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 20)
        Me.Label2.TabIndex = 44
        Me.Label2.Text = "Backlight"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label9.Location = New System.Drawing.Point(66, 210)
        Me.Label9.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label9.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 20)
        Me.Label9.TabIndex = 51
        Me.Label9.Text = "AutoFocus"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label14.Location = New System.Drawing.Point(66, 340)
        Me.Label14.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label14.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(100, 20)
        Me.Label14.TabIndex = 56
        Me.Label14.Text = "Iris Down"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label7.Location = New System.Drawing.Point(66, 158)
        Me.Label7.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label7.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 20)
        Me.Label7.TabIndex = 49
        Me.Label7.Text = "Zoom Near 2"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Location = New System.Drawing.Point(66, 54)
        Me.Label3.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label3.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 20)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "Zoom Far 3"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label10.Location = New System.Drawing.Point(66, 236)
        Me.Label10.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label10.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(100, 20)
        Me.Label10.TabIndex = 52
        Me.Label10.Text = "Focus Far"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label13.Location = New System.Drawing.Point(66, 314)
        Me.Label13.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label13.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 20)
        Me.Label13.TabIndex = 55
        Me.Label13.Text = "Iris Up"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label11.Location = New System.Drawing.Point(66, 262)
        Me.Label11.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label11.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 20)
        Me.Label11.TabIndex = 53
        Me.Label11.Text = "Focus Near"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Location = New System.Drawing.Point(66, 79)
        Me.Label4.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label4.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 20)
        Me.Label4.TabIndex = 46
        Me.Label4.Text = "Zoom Far 2"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.Location = New System.Drawing.Point(66, 106)
        Me.Label5.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label5.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 20)
        Me.Label5.TabIndex = 47
        Me.Label5.Text = "Zoom Far 1"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label12.Location = New System.Drawing.Point(66, 288)
        Me.Label12.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label12.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 20)
        Me.Label12.TabIndex = 54
        Me.Label12.Text = "Auto Iris"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.BurlyWood
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label49)
        Me.Panel2.Controls.Add(Me.Label41)
        Me.Panel2.Controls.Add(Me.TextBox48)
        Me.Panel2.Controls.Add(Me.Label40)
        Me.Panel2.Controls.Add(Me.TextBox40)
        Me.Panel2.Controls.Add(Me.TextBox39)
        Me.Panel2.Controls.Add(Me.Label39)
        Me.Panel2.Controls.Add(Me.Label38)
        Me.Panel2.Controls.Add(Me.Label37)
        Me.Panel2.Controls.Add(Me.TextBox38)
        Me.Panel2.Controls.Add(Me.TextBox37)
        Me.Panel2.Controls.Add(Me.TextBox36)
        Me.Panel2.Controls.Add(Me.Label36)
        Me.Panel2.Controls.Add(Me.TextBox35)
        Me.Panel2.Controls.Add(Me.Label35)
        Me.Panel2.Controls.Add(Me.TextBox34)
        Me.Panel2.Controls.Add(Me.Label34)
        Me.Panel2.Controls.Add(Me.TextBox33)
        Me.Panel2.Controls.Add(Me.TextBox26)
        Me.Panel2.Controls.Add(Me.TextBox25)
        Me.Panel2.Controls.Add(Me.TextBox24)
        Me.Panel2.Controls.Add(Me.TextBox23)
        Me.Panel2.Controls.Add(Me.TextBox22)
        Me.Panel2.Controls.Add(Me.TextBox21)
        Me.Panel2.Controls.Add(Me.TextBox20)
        Me.Panel2.Controls.Add(Me.TextBox19)
        Me.Panel2.Controls.Add(Me.TextBox18)
        Me.Panel2.Controls.Add(Me.TextBox17)
        Me.Panel2.Controls.Add(Me.Label26)
        Me.Panel2.Controls.Add(Me.Label25)
        Me.Panel2.Controls.Add(Me.Label24)
        Me.Panel2.Controls.Add(Me.Label23)
        Me.Panel2.Controls.Add(Me.Label22)
        Me.Panel2.Controls.Add(Me.Label21)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.Label17)
        Me.Panel2.Location = New System.Drawing.Point(208, 28)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(167, 505)
        Me.Panel2.TabIndex = 50
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label41.Location = New System.Drawing.Point(66, 444)
        Me.Label41.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label41.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(100, 20)
        Me.Label41.TabIndex = 108
        Me.Label41.Text = "Frame FWD"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label40.Location = New System.Drawing.Point(66, 419)
        Me.Label40.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label40.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(100, 20)
        Me.Label40.TabIndex = 107
        Me.Label40.Text = "Frame REV"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox40
        '
        Me.TextBox40.Location = New System.Drawing.Point(15, 445)
        Me.TextBox40.Name = "TextBox40"
        Me.TextBox40.Size = New System.Drawing.Size(34, 20)
        Me.TextBox40.TabIndex = 106
        '
        'TextBox39
        '
        Me.TextBox39.Location = New System.Drawing.Point(15, 419)
        Me.TextBox39.Name = "TextBox39"
        Me.TextBox39.Size = New System.Drawing.Size(34, 20)
        Me.TextBox39.TabIndex = 105
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label39.Location = New System.Drawing.Point(66, 394)
        Me.Label39.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label39.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(100, 20)
        Me.Label39.TabIndex = 104
        Me.Label39.Text = "X 9"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label38.Location = New System.Drawing.Point(66, 371)
        Me.Label38.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label38.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(100, 20)
        Me.Label38.TabIndex = 103
        Me.Label38.Text = "X 1/5"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label37.Location = New System.Drawing.Point(66, 346)
        Me.Label37.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label37.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(100, 20)
        Me.Label37.TabIndex = 102
        Me.Label37.Text = "X 1/10"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox38
        '
        Me.TextBox38.Location = New System.Drawing.Point(15, 392)
        Me.TextBox38.Name = "TextBox38"
        Me.TextBox38.Size = New System.Drawing.Size(34, 20)
        Me.TextBox38.TabIndex = 101
        '
        'TextBox37
        '
        Me.TextBox37.Location = New System.Drawing.Point(15, 366)
        Me.TextBox37.Name = "TextBox37"
        Me.TextBox37.Size = New System.Drawing.Size(34, 20)
        Me.TextBox37.TabIndex = 100
        '
        'TextBox36
        '
        Me.TextBox36.Location = New System.Drawing.Point(15, 340)
        Me.TextBox36.Name = "TextBox36"
        Me.TextBox36.Size = New System.Drawing.Size(34, 20)
        Me.TextBox36.TabIndex = 99
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label36.Location = New System.Drawing.Point(66, 317)
        Me.Label36.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label36.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(100, 20)
        Me.Label36.TabIndex = 98
        Me.Label36.Text = "SD Write"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox35
        '
        Me.TextBox35.Location = New System.Drawing.Point(15, 314)
        Me.TextBox35.Name = "TextBox35"
        Me.TextBox35.Size = New System.Drawing.Size(34, 20)
        Me.TextBox35.TabIndex = 97
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label35.Location = New System.Drawing.Point(66, 290)
        Me.Label35.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label35.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(100, 20)
        Me.Label35.TabIndex = 96
        Me.Label35.Text = "Photo Capture"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox34
        '
        Me.TextBox34.Location = New System.Drawing.Point(15, 290)
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.Size = New System.Drawing.Size(34, 20)
        Me.TextBox34.TabIndex = 95
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label34.Location = New System.Drawing.Point(66, 264)
        Me.Label34.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label34.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(100, 20)
        Me.Label34.TabIndex = 94
        Me.Label34.Text = "Mode Switch"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox33
        '
        Me.TextBox33.Location = New System.Drawing.Point(15, 264)
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.Size = New System.Drawing.Size(34, 20)
        Me.TextBox33.TabIndex = 93
        '
        'TextBox26
        '
        Me.TextBox26.Location = New System.Drawing.Point(15, 238)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Size = New System.Drawing.Size(34, 20)
        Me.TextBox26.TabIndex = 92
        '
        'TextBox25
        '
        Me.TextBox25.Location = New System.Drawing.Point(15, 211)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(34, 20)
        Me.TextBox25.TabIndex = 91
        '
        'TextBox24
        '
        Me.TextBox24.Location = New System.Drawing.Point(15, 185)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Size = New System.Drawing.Size(34, 20)
        Me.TextBox24.TabIndex = 90
        '
        'TextBox23
        '
        Me.TextBox23.Location = New System.Drawing.Point(15, 159)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Size = New System.Drawing.Size(34, 20)
        Me.TextBox23.TabIndex = 89
        '
        'TextBox22
        '
        Me.TextBox22.Location = New System.Drawing.Point(15, 133)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New System.Drawing.Size(34, 20)
        Me.TextBox22.TabIndex = 88
        '
        'TextBox21
        '
        Me.TextBox21.Location = New System.Drawing.Point(15, 105)
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Size = New System.Drawing.Size(34, 20)
        Me.TextBox21.TabIndex = 87
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(15, 80)
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New System.Drawing.Size(34, 20)
        Me.TextBox20.TabIndex = 86
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(15, 54)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(34, 20)
        Me.TextBox19.TabIndex = 85
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(15, 28)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(34, 20)
        Me.TextBox18.TabIndex = 84
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(15, 4)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(34, 20)
        Me.TextBox17.TabIndex = 74
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label26.Location = New System.Drawing.Point(66, 237)
        Me.Label26.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label26.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(100, 20)
        Me.Label26.TabIndex = 83
        Me.Label26.Text = "Power Off"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label25.Location = New System.Drawing.Point(66, 210)
        Me.Label25.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label25.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(100, 20)
        Me.Label25.TabIndex = 82
        Me.Label25.Text = "Power On"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label24.Location = New System.Drawing.Point(66, 184)
        Me.Label24.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label24.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(100, 20)
        Me.Label24.TabIndex = 81
        Me.Label24.Text = "Stop"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label23.Location = New System.Drawing.Point(66, 158)
        Me.Label23.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label23.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(100, 20)
        Me.Label23.TabIndex = 80
        Me.Label23.Text = "Pause"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label22.Location = New System.Drawing.Point(66, 130)
        Me.Label22.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label22.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(100, 20)
        Me.Label22.TabIndex = 79
        Me.Label22.Text = "FFwd"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label21.Location = New System.Drawing.Point(66, 104)
        Me.Label21.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label21.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(100, 20)
        Me.Label21.TabIndex = 78
        Me.Label21.Text = "Fwd Search"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label20.Location = New System.Drawing.Point(66, 79)
        Me.Label20.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label20.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(100, 20)
        Me.Label20.TabIndex = 77
        Me.Label20.Text = "PLay"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label19.Location = New System.Drawing.Point(66, 55)
        Me.Label19.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label19.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(100, 20)
        Me.Label19.TabIndex = 76
        Me.Label19.Text = "Rew Search"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label18.Location = New System.Drawing.Point(66, 29)
        Me.Label18.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label18.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(100, 20)
        Me.Label18.TabIndex = 75
        Me.Label18.Text = "FRew"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label17.Location = New System.Drawing.Point(66, 3)
        Me.Label17.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label17.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(100, 20)
        Me.Label17.TabIndex = 74
        Me.Label17.Text = "Eject"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.BurlyWood
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label47)
        Me.Panel3.Controls.Add(Me.TextBox46)
        Me.Panel3.Controls.Add(Me.Label46)
        Me.Panel3.Controls.Add(Me.TextBox45)
        Me.Panel3.Controls.Add(Me.Label45)
        Me.Panel3.Controls.Add(Me.TextBox44)
        Me.Panel3.Controls.Add(Me.Label44)
        Me.Panel3.Controls.Add(Me.TextBox32)
        Me.Panel3.Controls.Add(Me.TextBox43)
        Me.Panel3.Controls.Add(Me.Label32)
        Me.Panel3.Controls.Add(Me.TextBox31)
        Me.Panel3.Controls.Add(Me.TextBox30)
        Me.Panel3.Controls.Add(Me.TextBox29)
        Me.Panel3.Controls.Add(Me.TextBox28)
        Me.Panel3.Controls.Add(Me.TextBox27)
        Me.Panel3.Controls.Add(Me.Label31)
        Me.Panel3.Controls.Add(Me.Label30)
        Me.Panel3.Controls.Add(Me.Label29)
        Me.Panel3.Controls.Add(Me.Label28)
        Me.Panel3.Controls.Add(Me.Label27)
        Me.Panel3.Location = New System.Drawing.Point(394, 28)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(167, 259)
        Me.Panel3.TabIndex = 50
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label47.Location = New System.Drawing.Point(66, 234)
        Me.Label47.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label47.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(100, 20)
        Me.Label47.TabIndex = 120
        Me.Label47.Text = "Date Disp OFF"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox46
        '
        Me.TextBox46.Location = New System.Drawing.Point(15, 235)
        Me.TextBox46.Name = "TextBox46"
        Me.TextBox46.Size = New System.Drawing.Size(34, 20)
        Me.TextBox46.TabIndex = 119
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label46.Location = New System.Drawing.Point(66, 209)
        Me.Label46.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label46.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(100, 20)
        Me.Label46.TabIndex = 118
        Me.Label46.Text = "Date Disp ON"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox45
        '
        Me.TextBox45.Location = New System.Drawing.Point(15, 209)
        Me.TextBox45.Name = "TextBox45"
        Me.TextBox45.Size = New System.Drawing.Size(34, 20)
        Me.TextBox45.TabIndex = 117
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label45.Location = New System.Drawing.Point(66, 183)
        Me.Label45.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label45.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(100, 20)
        Me.Label45.TabIndex = 116
        Me.Label45.Text = "Counter Display"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox44
        '
        Me.TextBox44.Location = New System.Drawing.Point(15, 184)
        Me.TextBox44.Name = "TextBox44"
        Me.TextBox44.Size = New System.Drawing.Size(34, 20)
        Me.TextBox44.TabIndex = 115
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label44.Location = New System.Drawing.Point(66, 158)
        Me.Label44.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label44.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(100, 20)
        Me.Label44.TabIndex = 114
        Me.Label44.Text = "Data Display"
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox32
        '
        Me.TextBox32.Location = New System.Drawing.Point(15, 133)
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.Size = New System.Drawing.Size(34, 20)
        Me.TextBox32.TabIndex = 103
        '
        'TextBox43
        '
        Me.TextBox43.Location = New System.Drawing.Point(15, 159)
        Me.TextBox43.Name = "TextBox43"
        Me.TextBox43.Size = New System.Drawing.Size(34, 20)
        Me.TextBox43.TabIndex = 113
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label32.Location = New System.Drawing.Point(66, 131)
        Me.Label32.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label32.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(100, 20)
        Me.Label32.TabIndex = 102
        Me.Label32.Text = "Assign 6"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox31
        '
        Me.TextBox31.Location = New System.Drawing.Point(15, 106)
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.Size = New System.Drawing.Size(34, 20)
        Me.TextBox31.TabIndex = 101
        '
        'TextBox30
        '
        Me.TextBox30.Location = New System.Drawing.Point(15, 80)
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.Size = New System.Drawing.Size(34, 20)
        Me.TextBox30.TabIndex = 100
        '
        'TextBox29
        '
        Me.TextBox29.Location = New System.Drawing.Point(15, 57)
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.Size = New System.Drawing.Size(34, 20)
        Me.TextBox29.TabIndex = 99
        '
        'TextBox28
        '
        Me.TextBox28.Location = New System.Drawing.Point(15, 31)
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.Size = New System.Drawing.Size(34, 20)
        Me.TextBox28.TabIndex = 98
        '
        'TextBox27
        '
        Me.TextBox27.Location = New System.Drawing.Point(15, 5)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.Size = New System.Drawing.Size(34, 20)
        Me.TextBox27.TabIndex = 93
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label31.Location = New System.Drawing.Point(66, 104)
        Me.Label31.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label31.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(100, 20)
        Me.Label31.TabIndex = 97
        Me.Label31.Text = "Assign 5"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label30.Location = New System.Drawing.Point(66, 80)
        Me.Label30.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label30.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(100, 20)
        Me.Label30.TabIndex = 96
        Me.Label30.Text = "Assign 4"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label29.Location = New System.Drawing.Point(66, 55)
        Me.Label29.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label29.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(100, 20)
        Me.Label29.TabIndex = 95
        Me.Label29.Text = "Assign 3"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label28.Location = New System.Drawing.Point(66, 28)
        Me.Label28.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label28.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(100, 20)
        Me.Label28.TabIndex = 94
        Me.Label28.Text = "Assign 2"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label27.Location = New System.Drawing.Point(66, 4)
        Me.Label27.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label27.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(100, 20)
        Me.Label27.TabIndex = 93
        Me.Label27.Text = "Assign 1"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SetDefault
        '
        Me.SetDefault.Location = New System.Drawing.Point(445, 391)
        Me.SetDefault.Name = "SetDefault"
        Me.SetDefault.Size = New System.Drawing.Size(80, 25)
        Me.SetDefault.TabIndex = 51
        Me.SetDefault.Text = "Set defaults"
        Me.SetDefault.UseVisualStyleBackColor = True
        '
        'GoControls1
        '
        Me.GoControls1.Location = New System.Drawing.Point(445, 420)
        Me.GoControls1.Name = "GoControls1"
        Me.GoControls1.Size = New System.Drawing.Size(80, 51)
        Me.GoControls1.TabIndex = 52
        Me.GoControls1.Text = "Go to controls"
        Me.GoControls1.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.BurlyWood
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label33)
        Me.Panel4.Controls.Add(Me.ListBox1)
        Me.Panel4.Location = New System.Drawing.Point(394, 290)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(167, 98)
        Me.Panel4.TabIndex = 51
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label33.Location = New System.Drawing.Point(66, 15)
        Me.Label33.MaximumSize = New System.Drawing.Size(100, 40)
        Me.Label33.MinimumSize = New System.Drawing.Size(100, 40)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(100, 40)
        Me.Label33.TabIndex = 104
        Me.Label33.Text = "Com Ports available on   this machine"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(15, 15)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(45, 69)
        Me.ListBox1.TabIndex = 54
        '
        'PrintForm
        '
        Me.PrintForm.Location = New System.Drawing.Point(445, 476)
        Me.PrintForm.Name = "PrintForm"
        Me.PrintForm.Size = New System.Drawing.Size(80, 25)
        Me.PrintForm.TabIndex = 53
        Me.PrintForm.Text = "Print"
        Me.PrintForm.UseVisualStyleBackColor = True
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label48.Location = New System.Drawing.Point(67, 472)
        Me.Label48.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label48.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(100, 20)
        Me.Label48.TabIndex = 122
        Me.Label48.Text = "Tape Review"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox47
        '
        Me.TextBox47.Location = New System.Drawing.Point(16, 472)
        Me.TextBox47.Name = "TextBox47"
        Me.TextBox47.Size = New System.Drawing.Size(34, 20)
        Me.TextBox47.TabIndex = 121
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label49.Location = New System.Drawing.Point(66, 473)
        Me.Label49.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Label49.MinimumSize = New System.Drawing.Size(100, 20)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(100, 20)
        Me.Label49.TabIndex = 124
        Me.Label49.Text = "VTR Record"
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox48
        '
        Me.TextBox48.Location = New System.Drawing.Point(15, 473)
        Me.TextBox48.Name = "TextBox48"
        Me.TextBox48.Size = New System.Drawing.Size(34, 20)
        Me.TextBox48.TabIndex = 123
        '
        'SetUp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(692, 545)
        Me.Controls.Add(Me.PrintForm)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.GoControls1)
        Me.Controls.Add(Me.SetDefault)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "SetUp"
        Me.Text = "Sony Remote Control by Nigel Ealand"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents SetDefault As System.Windows.Forms.Button
    Friend WithEvents GoControls1 As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents TextBox26 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox24 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox23 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox22 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox21 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox20 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox27 As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents TextBox31 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox30 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox29 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintForm As System.Windows.Forms.Button
    Friend WithEvents TextBox32 As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents TextBox33 As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents TextBox34 As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TextBox35 As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TextBox40 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox39 As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents TextBox38 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox37 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox36 As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents TextBox41 As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents TextBox42 As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents TextBox46 As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents TextBox45 As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents TextBox44 As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents TextBox43 As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents TextBox47 As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents TextBox48 As System.Windows.Forms.TextBox
End Class
