﻿

Module Module1

    Public ComPortName As String
    Public Thours As Integer
    Public Tminutes As Integer
    Public Tsecondes As Integer
    Public Tframes As Integer
    Public FramesByte As Integer
    Public FrameInt As Byte
    Public FrameDec As Integer
    Public TframeSign As Boolean
    Public TframeCounter As Boolean
    Public buffers As String
    Public car As String
    Public VTR_MODE As String
    Public counter As String
    Public ContCode As String
    Public powerLOW As Integer
    Public Sign As String


    Public MODE_PLAY, MODE_STOP, MODE_EJECT, MODE_REVIEW, MODE_CUE_ON, MODE_STARTSTOP As String
    Public MODE_TELE1, MODE_TELE2, MODE_TELE3, MODE_WIDE1, MODE_WIDE2, MODE_WIDE3 As String
    Public MODE_REWIND, MODE_FF, MODE_REC, MODE_PAUSE, MODE_SLOW1_10, MODE_SLOW1_5, MODE_HIGHDSPEED, MODE_HIGH_SPEED, MODE_OFF, MODE_ON As String
    Public MODE_AutoFocus, MODE_FocusFar, MODE_FocusNear, MODE_AutoIris, MODE_IrisUp, MODE_IrisDown, Mode_Backlight, MODE_SDWrite, Mode_PhotoCapture As String
    Public MODE_AutoWB, MODE_WBSet, MODE_TENTH, MODE_FIFTH, MODE_NINE, MODE_FRAMEFWD, MODE_FRAMEREV As String
    Public MODE_Switch, MODE_TCRC, MODE_TAPE_END, MODE_DATA, MODE_COUNTER, MODE_DATEON, MODE_DATEOFF As String
    Public MODE_Assign1, MODE_Assign2, MODE_Assign3, MODE_Assign4, MODE_Assign5, MODE_Assign6 As String
    Public MODE_RecReview, MODE_VTRRec As String
    Public WithEvents myport As New System.IO.Ports.SerialPort

    Public Sub Initial_Port()
        myport.Close()
        'close_port()
        With myport
            .PortName = ComPortName                 '// Uses COM1-4 
            .BaudRate = 9600                        '// 9600 baud rate 
            .DataBits = 8                           '// 8 data bits
            .StopBits = IO.Ports.StopBits.One       '// 1 Stop bit 
            .Parity = IO.Ports.Parity.None          '// No Parity 
            .ReceivedBytesThreshold = 4             '//number of bytes received
            .Handshake = IO.Ports.Handshake.None

        End With
        '// Initializes and Open 
        Try
            If Not myport.IsOpen Then
                myport.Open()
            End If

            ResetInterface()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub


    Public Sub ResetInterface()
        Dim A As Integer

        ' Turn on camera 
        send_car("ATSP")
        A = 0
        Do Until A = 1000
            A = A + 1
        Loop

        ' Turn off echo so the send codes do not return 
        send_car("ATE0")
        A = 0
        Do Until A = 1000
            A = A + 1
        Loop
        ' Return data only when the status changes
        send_car("ATC0")
        A = 0
        Do Until A = 1000
            A = A + 1
        Loop
        A = 0
        ' Sends the commands 15 times
        send_car("ATRF")
        Do Until A = 1000
            A = A + 1
        Loop
        A = 0
        ' removed B0 + B1 duplication
        send_car("ATD0")

        Do Until A = 1000
            A = A + 1
        Loop
        'Send VTR stop
        send_car(MODE_STOP)

    End Sub

    Public Sub send_car(ByVal car As String)

        'sends code to the camera
        myport.Write(car & Chr(13))

    End Sub
    Public Sub Send_Car_Fast(ByVal car As String)
        myport.Write(car & Chr(13))
    End Sub

    Public Sub Status()

        ' Turn on camera status
        myport.Write("AT?" & Chr(13))
    End Sub

    Public Sub myport_DataReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles myport.DataReceived

        buffers = myport.ReadLine

        If Len(buffers) > 7 Then ' filters only true camera status and sends it to decode
            decode_mode()
        End If

    End Sub



    Public Sub close_port()

        If myport.IsOpen Then
            myport.Close()
            myport.Dispose()
            myport = Nothing
        End If

    End Sub

    Public Sub decode_mode()
        'decode the status from the camera 
        ' Important to note is that the ELM624 strips bytes 0-3. It returns only 4-7.
        ' Therefore bytes 4-7 become bytes 0-3, each have 8 bits or 2 characters
        ' So if you want the low nibble of byte 4 it would be 4th character in the train.
        ' use Left$(buffers, 4, 1) grabs that nibble

        Dim Powerbyte As String

        Select Case (Left$(buffers, 2))

            Case "01" : VTR_MODE = "EJECT"
            Case "02" : VTR_MODE = "STOP"
            Case "03" : VTR_MODE = "FF"
            Case "04" : VTR_MODE = "RECORD"
            Case "06" : VTR_MODE = "PLAY"
            Case "07" : VTR_MODE = "STILL"
            Case "14" : VTR_MODE = "PAUSE-REC"
            Case "17" : VTR_MODE = "FRAME FWD"
            Case "21" : VTR_MODE = "EJECTING"
            Case "22" : VTR_MODE = "CASSETTE BUSY"
            Case "26" : VTR_MODE = "X1 FWD"
            Case "27" : VTR_MODE = "1/5 FWD"
            Case "31" : VTR_MODE = "UNLOAD"
            Case "32" : VTR_MODE = "LOW POWER"
            Case "36" : VTR_MODE = "REVERSE PLAY"
            Case "37" : VTR_MODE = "1/5 REV"
            Case "42" : VTR_MODE = "DEW STOP"
            Case "46" : VTR_MODE = "CUE"
            Case "47" : VTR_MODE = "1/10 FWD"
            Case "56" : VTR_MODE = "REVIEW"
            Case "57" : VTR_MODE = "1/10 REV"
            Case "62" : VTR_MODE = "TAPE END"
            Case "66" : VTR_MODE = "PLAY X2/X3"
            Case "67" : VTR_MODE = "FRAME FWD"
            Case "72" : VTR_MODE = "TAPE TOP"
            Case "76" : VTR_MODE = "REVERSE PLAY X2/X3"
            Case "77" : VTR_MODE = "FRAME REV"
            Case "83" : VTR_MODE = "REWIND"
            Case "86" : VTR_MODE = "X9 FWD"
            Case "96" : VTR_MODE = "X9 REV"
            Case "97" : VTR_MODE = "PLAY PAUSE"
            Case "C6" : VTR_MODE = "X14 FWD"
            Case "D6" : VTR_MODE = "X14 REV"
        End Select

        Remote.StatusLabel1.Text = VTR_MODE

        'Detect whether the battery is low. This is in where Byte5 bit2 is set
        Powerbyte = Mid$(buffers, 4, 1) ' grabs the low nibble
        powerLOW = Val("&H" & Powerbyte) ' converts hex to decimal

        Select Case (Mid$(buffers, 3, 1))
            'Selects Byte 5 for the guide codes for Bytes 4, 6 & 7
            '1 = Status(Byte4), 2 = Counter(Bytes 6&7), 3 = Timecode(Bytes 6&7) 

            Case "1"


                'Decimal Counter
            Case "2" : Remote.FramesLabel.Visible = False
                Remote.FramesLabel1.Visible = False
                Tsecondes = Val(Mid$(buffers, 5, 2))
                Tminutes = Val(Mid$(buffers, 7, 2))
                counter = Mid$(buffers, 7, 2)

                'Recovers the minutes and seconds
            Case "3" : Tsecondes = Val(Mid$(buffers, 5, 2))
                Tminutes = Val(Mid$(buffers, 7, 2))
                counter = Mid$(buffers, 7, 2)

                'Recovers the frames and the hours
            Case "4"
                ' This is to work out the whether it is time code or a time counter
                ' If the bit2 in the upper byte is set then it is a Time Code
                ' If the bit3 in the upper byte is set then the value is negative 
                Tframes = 0
                Thours = Val(Mid$(buffers, 5, 2))
                Tframes = Val(Mid$(buffers, 7, 2))
                Tframes = Tframes - 40

        End Select

    End Sub



End Module
