﻿Public Class Remote

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        ' This activates a sequence to update the Remote Form with satus
        FormTimer.Start()

    End Sub




    Private Sub SetUp1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SetUp1.Click
        SetUp.Visible = True
        Me.Visible = False

    End Sub

    Private Sub TCSWITCH_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TCSWITCH.Click
        send_car(MODE_TCRC)
    End Sub

    Private Sub ModeSW1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ModeSW1.Click
        send_car(MODE_Switch)

    End Sub


    Private Sub ZoomIn_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ZoomIn.MouseDown
        ContCode = MODE_TELE1
        ZoomTimer.Start()
    End Sub

    Private Sub ZoomIn_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ZoomIn.MouseUp
        ZoomTimer.Stop()
    End Sub

    Private Sub ZomOut_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ZomOut.MouseDown
        ContCode = MODE_WIDE1
        ZoomTimer.Start()
    End Sub

    Private Sub ZomOut_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ZomOut.MouseUp
        ZoomTimer.Stop()
    End Sub


    Private Sub Power1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Power1.Click
        If Power1.Text = "Power ON" Then
            send_car(MODE_OFF)
            Power1.Text = "Power OFF"
        Else
            send_car(MODE_ON)
            Power1.Text = "Power ON"
        End If
    End Sub


    'VTR Modes
    Private Sub Play1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Play1.Click
        send_car(MODE_PLAY)

    End Sub
    Private Sub Stop1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Stop1.Click
        send_car(MODE_STOP)

    End Sub
    Private Sub Forward1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Forward1.Click
        send_car(MODE_CUE_ON)
    End Sub
    Private Sub Rewind1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rewind1.Click
        send_car(MODE_REVIEW)
    End Sub
    Private Sub Forward2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Forward2.Click
        send_car(MODE_FF)
    End Sub
    Private Sub Rewind2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rewind2.Click
        send_car(MODE_REWIND)
    End Sub
    Private Sub Pause1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Pause1.Click
        send_car(MODE_PAUSE)
    End Sub
    Private Sub Eject1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eject1.Click
        send_car(MODE_EJECT)
    End Sub
    Private Sub Tenth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tenth.Click
        send_car(MODE_TENTH)
    End Sub

    Private Sub Fifth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fifth.Click
        send_car(MODE_FIFTH)
    End Sub

    Private Sub NineTimes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles NineTimes.Click
        send_car(MODE_NINE)
    End Sub

    Private Sub FrameREV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles FrameREV.Click
        send_car(MODE_FRAMEREV)
    End Sub

    Private Sub FrameFWD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles FrameFWD.Click
        send_car(MODE_FRAMEFWD)
    End Sub


    Private Sub TapeEnd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TapeEnd.Click
        send_car(MODE_TAPE_END)
    End Sub

    Private Sub VTRRecord_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles VTRRecord.Click
        send_car(MODE_VTRRec)
    End Sub


    'Camera buttons
    Private Sub Record1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Record1.Click
        send_car(MODE_REC)
    End Sub

    Private Sub Autofocus1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Autofocus1.Click
        send_car(MODE_AutoFocus)
    End Sub
    Private Sub FocusFar1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles FocusFar1.MouseDown
        ContCode = MODE_FocusFar
        FocusTimer.Start()
    End Sub

    Private Sub FocusFar1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles FocusFar1.MouseUp
        FocusTimer.Stop()
    End Sub

    Private Sub FocusNear1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles FocusNear1.MouseDown
        ContCode = MODE_FocusNear
        FocusTimer.Start()
    End Sub

    Private Sub FocusNear1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles FocusNear1.MouseUp
        FocusTimer.Stop()
    End Sub

    Private Sub Backlight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Backlight.Click
        send_car(Mode_Backlight)
    End Sub


    Private Sub PhotoCapture_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PhotoCapture.Click
        send_car(MODE_Photocapture)
    End Sub

    Private Sub SDWrite_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SDWrite.Click
        send_car(MODE_SDWrite)
    End Sub

    Private Sub RecordReview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RecordReview.Click
        send_car(MODE_RecReview)
    End Sub


    ' Display Features

    Private Sub DataButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataButton.Click
        send_car(MODE_DATA)
    End Sub

    Private Sub CounterButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CounterButton.Click
        send_car(MODE_COUNTER)
    End Sub

    Private Sub DateDisButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateDisButton1.Click
        send_car(MODE_DATEON)
    End Sub

    Private Sub DateDisButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateDisButton2.Click
        send_car(MODE_DATEOFF)
    End Sub


    'Assignable buttons

    Private Sub Assign1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Assign1.MouseDown
        send_car(MODE_Assign1)
    End Sub

    Private Sub Assign2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Assign2.MouseDown
        send_car(MODE_Assign2)
    End Sub

    Private Sub Assign3_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Assign3.MouseDown
        send_car(MODE_Assign3)
    End Sub

    Private Sub Assign4_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Assign4.MouseDown
        send_car(MODE_Assign4)
    End Sub

    Private Sub Assign5_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Assign5.MouseDown
        send_car(MODE_Assign5)
    End Sub

    Private Sub Assign6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Assign6.Click
        send_car(MODE_Assign6)
    End Sub


    Private Sub FocusTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles FocusTimer.Tick
        Send_Car_Fast(ContCode)
    End Sub

    Private Sub ZoomTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZoomTimer.Tick
        Send_Car_Fast(ContCode)
    End Sub


    Private Sub FormTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles FormTimer.Tick
        StatusLabel1.Text = VTR_MODE & vbLf & buffers

        'Detect whether the battery is low. This is in where Byte5 bit2 is set
        If (powerLOW > 3 And powerLOW < 8) Or (powerLOW > 11) Then 'these are the limits where bit2 is set
            BatteryLabel1.Text = "Battery Low"
            BatteryLabel1.Visible = True
        Else
            BatteryLabel1.Text = ""
            BatteryLabel1.Visible = False
        End If

        If (counter) = "FF" Then
            HoursLabel.Text = "--"
            MinutesLabel.Text = "--"
            SecondsLabel.Text = "--"
            FramesLabel.Text = "--"
        Else
            HoursLabel.Text = Format(Thours, "0" & 0.0#)
            MinutesLabel.Text = Format(Tminutes, "0" & 0.0#)
            SecondsLabel.Text = Format(Tsecondes, "0" & 0.0#)



            If Tframes > -1 And Tframes <= 26 Then 'Timecode

                Me.FramesLabel.Visible = True
                Me.FramesLabel1.Visible = True
                Me.ColonLabel1.Visible = True
                Me.TimecodeLabel1.Text = "Time Code"
                Me.SignLabel.Text = "" ' Timecode frame count can only be +'ve
                Me.FramesLabel.Text = Format(Tframes, "0" & 0.0#)

            Else ' Counter
                Me.FramesLabel.Visible = False
                Me.FramesLabel1.Visible = False
                Me.ColonLabel1.Visible = False
                Me.TimecodeLabel1.Text = "Time Counter"
                If Tframes >= 26 Then
                    Me.SignLabel.Text = "-"
                Else
                    Me.SignLabel.Text = ""
                End If

            End If


        End If


    End Sub



    Private Sub Remote_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        SetUp.Close()
        close_port()
    End Sub






End Class
